import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  userInfo: FormGroup;
  isValid = false;
  userDtls = [];
  showtable = false;
  searchText;
  hobbies = []
  constructor(
    private fb: FormBuilder
  ) {
      this.userInfo = this.fb.group({
        name: [null, Validators.required],
        age: [null, Validators.required],
        gender: [null, Validators.required],
        hobbies: [null, Validators.required],
        checked: [null, Validators.required],
      })
   }

  ngOnInit(): void {
    this.userInfo.valueChanges.subscribe(result => {
      this.isValid = this.userInfo.valid ? true : false
    })
  }


  onClickAdd() {
    let hobbi = {name: this.userInfo.get('hobbies').value}
    this.hobbies.push(hobbi)
    let obj = this.userInfo.value;
    obj.hobbies =  this.hobbies;
    this.userDtls.push(obj);
    this.showtable =  this.userDtls && this.userDtls.length ? true : false;
    this.userInfo.reset();
  }

  onClickReset() {
    this.userInfo.reset();
  }

  search(event) {
    let input = event.target.value;
    this.userDtls = this.userDtls.filter(function(tag) {
      return tag.name.indexOf(input) >= 0;
  }); 
  }

  delete(index) {
    this.userDtls.splice(index, 1);
    this.showtable =  this.userDtls && this.userDtls.length ? true : false;
  }

  addHobbi(input) {
    if(input == 'add') {
      this.hobbies.push({name: this.userInfo.get('hobbies').value})
      this.userInfo.get('hobbies').setValue(null);
    }else {
      let i = this.hobbies.indexOf(input)
      this.hobbies = this.hobbies.splice(i, 1);
    }
  }
}
